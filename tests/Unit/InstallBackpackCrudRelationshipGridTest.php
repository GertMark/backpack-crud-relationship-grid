<?php

namespace GertMark\BackpackCrudRelationshipGrid\Tests\Unit;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;
use GertMark\BackpackCrudRelationshipGrid\Tests\TestCase;

class InstallBackpackCrudRelationshipGridTest extends TestCase
{
    /** @test */
    function the_install_command_copies_the_configuration()
    {
        // make sure we're starting from a clean state
        if (File::exists(config_path('backpack_crud_relationship_grid.php'))) {
            unlink(config_path('backpack_crud_relationship_grid.php'));
        }

        $this->assertFalse(File::exists(config_path('backpack_crud_relationship_grid.php')));

        Artisan::call('relationshipgrid:install');
        
        $this->assertTrue(File::exists(config_path('backpack_crud_relationship_grid.php')));
    }
}
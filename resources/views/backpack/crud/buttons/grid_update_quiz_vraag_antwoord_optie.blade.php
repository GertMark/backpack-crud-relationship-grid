@if ($crud->hasAccess('grid_update'))
	<!-- Single edit button -->
	<a href="{{ url($crud->route.'/'.$entry->getKey().'/edit').'?quiz_id='.request()->quiz_id.'&quiz_vraag_id='.request()->quiz_vraag_id }}" class="btn btn-sm btn-link"><i class="la la-edit"></i> {{ trans('backpack::crud.edit') }}</a>
@endif
<?php

namespace GertMark\BackpackCrudRelationshipGrid\Console;

use Illuminate\Console\Command;

class InstallBackpackCrudRelationshipGridPackage extends Command
{
    protected $signature = 'relationshipgrid:install';

    protected $description = 'Install the BackpackCrudRelationshipGrid';

    public function handle()
    {
        $this->info('Installing BackpackCrudRelationshipGrid...');

        $this->info('Publishing configuration...');

        $this->call('vendor:publish', [
            '--provider' => "GertMark\BackpackCrudRelationshipGrid\BackpackCrudRelationshipGridServiceProvider",
            '--tag' => "config"
        ]);
        $this->info('Publishing views...');

        $this->call('vendor:publish', [
            '--provider' => "GertMark\BackpackCrudRelationshipGrid\BackpackCrudRelationshipGridServiceProvider",
            '--tag' => "backpack_grid_views"
        ]);

        $this->info('Installed BackpackCrudRelationshipGrid');
    }
}
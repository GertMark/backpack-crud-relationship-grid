<?php

namespace GertMark\BackpackCrudRelationshipGrid\Console;

use Illuminate\Console\Command;

class InitRelationGridCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'init_relation_grid
                            {entity1 : base entity, has one to many relationship to entity2}
                            {entity2 : relation entity, has belongsto relationship to entity1}
                            {--mode= : check_requirements}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'creates a relationship tab in the edit view of backpack crud';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //check model and crudcontroller entity1 exists
        if (!$this->modelExists($this->argument('entity1'))) {
            $this->error("model for entity1:".$this->argument('entity1')." does not exist");
            return 1;  
        }

        if(!$this->hasManyExists()){
            $this->info("hasMany method:".$this->argument('entity2')." not detected in ".$this->argument('entity1')." model. Make sure your models are correct before you proceed");
        }

        //check model and crudcontroller entity2 exists
        if (!$this->modelExists($this->argument('entity2'))) {
            $this->error("model for entity2:".$this->argument('entity2')." does not exist");
            return 1;  
        }
        if(!$this->belongsToExists()){
             $this->info("belongTo method:".$this->argument('entity1')." not detected in ".$this->argument('entity2')." model. Make sure your models are correct before you proceed");
        }

        //check backpack crud entity1 is called
        if (!$this->crudControllerExists($this->argument('entity1'))) {
            $this->error("crudController for entity1:".$this->argument('entity1')." does not exist");
            return 1;  
        }
        
        //check backpack crud entity2 is called
        if (!$this->crudControllerExists($this->argument('entity2'))) {
            $this->error("crudController for entity2:".$this->argument('entity2')." does not exist");
            return 1;  
        }

        if($this->option('mode')=='check_requirements'){
            return;
        }
        
        /** package utilities */
        
        //check if views/vendor/backpack/crud/fields/relationship.blade.php extists. ifnot create it

        //check if views/vendor/backpack/crud/grid.blade.php extists. ifnot create it

        //check if views/vendor/backpack/crud/grid_wrapper.blade.php extists. ifnot create it

        //check if app/Http/Controllers/Operations/GridOperation.php exists. ifnot create it
        
        /** end package utilities */
        
        //in app/Http/Controllers/Admin/{Model1}CrudController.php
        // add use use \Backpack\CRUD\app\Http\Controllers\Operations\InlineCreateOperation;
        $this->addUseInlineCreateOperation();
        //      add use \GertMark\BackpackCrudRelationshipGrid\Http\Controllers\Operations;
        $this->addUseGridOperationModel1CrudController();
        //  in setup()
        //      add $this->handleRelationModelController('{Model2}CrudController',__NAMESPACE__);
        $this->addHandleRelationModelControllerToSetup();
        //  in setupUpdateOperation
              // add $this->crud->addFields(static::getFieldsArrayForGridTab(    $this->{Model2}Controller,
              //                                                       $this->{Model2}Controller->getCrud(),
              //                                                       '{tab_titel}}',
              //                                                       '{entity2}',
              //                                                       [{include_main_form_fields}}]
              //                                                   )
              //               );
        $this->addAddFieldsToSetupUpdateOperation();
        //  in app/Http/Controllers/Admin/{Model2}CrudController.php
        //  use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation{
        //     search as traitSearch;
        // }
        $this->addTraitSearchModel2CrudController();

        //  in app/Http/Controllers/Admin/{Model2}CrudController.php
        //      use \GertMark\BackpackCrudRelationshipGrid\Http\Controllers\Operations;        
        //      public $model = '\App\Models\{Model2}';
        //      public $entity = '{entity}';
        //      public $entityName = '{entity}';
        //      public $entityNames = '{entity}s';
        $this->addUseGridOperationModel2CrudController();
        $this->addPublicVars();

        //
        //public function search(){
        //     if(!is_null(request()->quiz_id)){
        //         $this->crud->query->where('quiz_id',request()->quiz_id);
        //     }
        //     return $this->traitSearch();
        // }
        $this->addSearchMethodModel2CrudController();

        return 0;
    }

    private function modelExists($entity){
        $camelCaseModelName = ucfirst(implode('', array_map('ucfirst', explode('_', $entity))));
        return class_exists('\App\Models\\'.$camelCaseModelName);
    }

    private function hasManyExists(){
        $camelCaseModelName = ucfirst(implode('', array_map('ucfirst', explode('_',$this->argument('entity1')))));
        $method = $this->argument('entity2');
        return method_exists('\App\Models\\'.$camelCaseModelName,$method);
    }

    private function belongsToExists(){
        $camelCaseModelName = ucfirst(implode('', array_map('ucfirst', explode('_',$this->argument('entity2')))));
        $method = $this->argument('entity1');
        return method_exists('\App\Models\\'.$camelCaseModelName,$method);
    }

    private function crudControllerExists($entity){
        $camelCaseModelName = ucfirst(implode('', array_map('ucfirst', explode('_', $entity))));
        $crudControllerName = $camelCaseModelName.'CrudController';
        return class_exists('\App\Http\Controllers\Admin\\'.$crudControllerName);
    }

    private function addUseGridOperationModel1CrudController(){
        $camelCaseModelName = ucfirst(implode('', array_map('ucfirst', explode('_',$this->argument('entity1'))))); 
        $this->addUseGridOperation($camelCaseModelName);
    }

    private function addUseGridOperationModel2CrudController(){
        $camelCaseModelName = ucfirst(implode('', array_map('ucfirst', explode('_',$this->argument('entity2'))))); 
        $this->addUseGridOperation($camelCaseModelName);
    }

    private function addUseInlineCreateOperation(){
        $camelCaseModelName = ucfirst(implode('', array_map('ucfirst', explode('_',$this->argument('entity2')))));
        $this->addUseStatementAfterUserShowOperationLine($camelCaseModelName,'use \Backpack\CRUD\app\Http\Controllers\Operations\InlineCreateOperation;');
    }

    private function addUseGridOperation($camelCaseModelName){
        $this->addUseStatementAfterUserShowOperationLine($camelCaseModelName,'use \GertMark\BackpackCrudRelationshipGrid\Http\Controllers\Operations\GridOperation;');
    }

    private function addUseStatementAfterUserShowOperationLine($camelCaseModelName,$useStatement){
        $file = app_path().'/Http/Controllers/Admin/'.$camelCaseModelName.'CrudController.php';
        $this->addLineAfter(    $file,
                                'use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation',
                                $useStatement
                            );
    }

    private function addTraitSearchModel2CrudController(){
        $camelCaseModelName = ucfirst(implode('', array_map('ucfirst', explode('_',$this->argument('entity2')))));
        $file = app_path().'/Http/Controllers/Admin/'.$camelCaseModelName.'CrudController.php';
        $this->replaceLine(     $file,
                                'use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;',
                                'use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation{'
                          );
        $this->addLinesAfter(    $file,
                                'use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation{',
                                [   '    search as traitSearch;',
                                    '}'
                                ]
                            );
    }

    private function addSearchMethodModel2CrudController(){
        $camelCaseModelName = ucfirst(implode('', array_map('ucfirst', explode('_',$this->argument('entity2')))));
        $file = app_path().'/Http/Controllers/Admin/'.$camelCaseModelName.'CrudController.php';
        $insertStrings = [
                            "public function search(){",
                            "       throw new \Exception('replace key and foreing key labels');",
                            "       //if(!is_null(request()->{key})){",
                            "       //    \$this->crud->query->where({foreign key},request()->{key});",
                            "       //}",
                            "       return \$this->traitSearch();",
                            "}"
                          ];
        $this->addLinesAfterMethod(    $file,
                                'setup()',
                                $insertStrings
                            );
    }

    private function addHandleRelationModelControllerToSetup(){
        $camelCaseModelName1 = ucfirst(implode('', array_map('ucfirst', explode('_',$this->argument('entity1')))));
        $camelCaseModelName2 = ucfirst(implode('', array_map('ucfirst', explode('_',$this->argument('entity2')))));
        $file = app_path().'/Http/Controllers/Admin/'.$camelCaseModelName1.'CrudController.php';
        $this->addLineAfter(    $file,
                                'CRUD::setEntityNameStrings',
                                "    \$this->handleRelationModelController('".$camelCaseModelName2."CrudController',__NAMESPACE__);"
                            ); 
    }

    private function addAddFieldsToSetupUpdateOperation(){
        $camelCaseModelName1 = ucfirst(implode('', array_map('ucfirst', explode('_',$this->argument('entity1')))));
        $camelCaseModelName2 = ucfirst(implode('', array_map('ucfirst', explode('_',$this->argument('entity2')))));
        $file = app_path().'/Http/Controllers/Admin/'.$camelCaseModelName1.'CrudController.php';
        $includeMainFormFields = $this->getIncludeMainFormFields();
        $insertStrings = [
                            "    \$this->crud->addFields(static::getFieldsArrayForGridTab(\$this->".$camelCaseModelName2."CrudController,",
                            "                                                             \$this->".$camelCaseModelName2."CrudController->getCrud(),",
                            "                                                             '".$this->argument('entity2')."',",
                            "                                                             '".$this->argument('entity2')."',",
                            "                                                             [".$includeMainFormFields."]",
                            "                                                         )",
                            "                       );"                            
                          ];
        $this->addLinesAfter(    $file,
                                '$this->setupCreateOperation()',
                                $insertStrings
                            );       
    }

    private function addPublicVars(){
        $camelCaseModelName2 = ucfirst(implode('', array_map('ucfirst', explode('_',$this->argument('entity2')))));
        $file = app_path().'/Http/Controllers/Admin/'.$camelCaseModelName2.'CrudController.php';
        $insertStrings = [
                            "public \$model = '\App\Models\\".$camelCaseModelName2."';",
                            "public \$entity = '".$this->argument('entity2')."';",
                            "public \$entityName = '".$this->argument('entity2')."';",
                            "public \$entityNames = '".$this->argument('entity2')."';",
        ];
        $this->addLinesAfter(    $file,
                                'use \GertMark\BackpackCrudRelationshipGrid\Http\Controllers\Operations',
                                $insertStrings
                            );  
    }

    private function addLineAfter($file,$findString,$insertString){
        $lines = $this->getLines($file);
        if($this->insertStringsInLines($lines,$insertString)){
            return;
        }
        $position = $this->getPosition($lines,$findString,$insertString);
        $doc = '';
        foreach ($lines as $key => $line) {
           $doc .= $line;
           if($key==$position&&$position){
                $doc .= '    '.$insertString.PHP_EOL;
           }
        }
        file_put_contents($file, $doc);        
    }

    private function addLinesAfter($file,$findString,$insertStrings){
        $lines = $this->getLines($file);
        if($this->insertStringsInLines($lines,$insertStrings[0])){
            return;
        }
        $position = $this->getPosition($lines,$findString,$insertStrings[0]);
        $doc = '';
        foreach ($lines as $key => $line) {
           $doc .= $line;
           if($key==$position&&$position){
                $doc = $this->addLinesToDoc($doc,$insertStrings);
           }
        }
        file_put_contents($file, $doc);
    }

    private function addLinesAfterMethod($file,$methodName,$insertStrings){
        $lines = $this->getLines($file);
        $position = $this->getPositionAfterMethod($lines,$methodName,$insertStrings[0]);
        $doc = '';
        foreach ($lines as $key => $line) {
           $doc .= $line;
           if($key==$position&&$position){
                $doc = $this->addLinesToDoc($doc,$insertStrings);
           }
        }
        file_put_contents($file, $doc);
    }

    private function replaceLine($file,$findString,$insertString){
        $lines = $this->getLines($file);
        $position = $this->getPosition($lines,$findString,$insertString);
        $doc = '';
        foreach ($lines as $key => $line) {
           if($key==$position&&$position){
                $line = str_replace($findString, $insertString, $line);
           }
           $doc .= $line;
        }
        file_put_contents($file, $doc);      
    }

    private function getLines($file){
        $handle = fopen($file,"r");
        $lines = [];
        while (!feof($handle)) {
            $line=fgets($handle);
            $lines[] = $line;
        }
        fclose($handle);
        return $lines; 
    }

    private function getPosition($lines,$findString,$insertString){
        $position = false;
        foreach ($lines as $key => $line) {
          if (strpos($line, $findString)!==false) {
                $position=$key;
          }
          if(trim($insertString)==''){
                continue;
          }
          if (strpos($line, trim($insertString))!==false) {
                $this->info($insertString.' already present');
                return false;
          }
        }
        return $position;
    }

    private function getPositionAfterMethod($lines,$methodName,$insertString){
        $position = false;
        $inMethod = false;
        $countBrackets = 0;
        $startCounting = false;
        foreach ($lines as $key => $line) {
          if (strpos($line, trim($insertString))!==false) {
                $this->info($insertString.' already present');
                return false;
          }
        }
        foreach ($lines as $key => $line) {
          if (strpos($line, $methodName)!==false) {
                $inMethod = true;
          }
          if (strpos($line, '{')!==false&&$inMethod) {
                $countBrackets++;
                $startCounting = true;
          }
          if (strpos($line, '}')!==false&&$inMethod) {
                $countBrackets--;
          }
          if($countBrackets===0&&$startCounting){
              $position = $key;
              break;
          }
        }
        return $position;
    }

    private function addLinesToDoc($doc,$insertStrings){
        foreach($insertStrings as $key => $insertString){
            $doc .= '    '.$insertString.PHP_EOL;
        }
        return $doc;
    }

    private function getIncludeMainFormFields(){
        $camelCaseModelName1 = ucfirst(implode('', array_map('ucfirst', explode('_',$this->argument('entity1')))));
        $camelCaseModelName2 = ucfirst(implode('', array_map('ucfirst', explode('_',$this->argument('entity2')))));
        $file = app_path().'/Models/'.$camelCaseModelName2.'.php';
        $lines = $this->getLines($file);
        $ids = [];
        foreach ($lines as $key => $line) {
            if (strpos($line, "\$this->belongsTo(".$camelCaseModelName1)!==false) {
                $ids[] = $this->getIdFromLine($line);
          }
        }
        return implode($ids,',');
    }

    private function getIdFromLine($line){
        $pos = strpos($line, "\$this->belongsTo(Symposium::class,")+34;
        $length = strlen($line);
        $countQuotes = 0;
        $end = false;
        $start = false;
        for($i=$pos;$i<=$length;$i++){
            $substr = substr($line,$i,($length-$i));
            if(substr($substr, 0,1)=="'"){
                $countQuotes++;
            }
            if($countQuotes==1&&substr($substr, 0,1)=="'"){
                $start = $i;
            }
            if($countQuotes==2){
                $end = $i+1;
                break;
            }
        }
        if($countQuotes!=2){
            return '';
        }
        $idLength = $end-$start;
        return substr($line,$start,$idLength);
    }

    private function insertStringsInLines($lines,$insertString){
        foreach($lines as $line){
            if(stristr($line, $insertString)){
                return true;
            }
        }
        return false;
    }
}

<?php
namespace GertMark\BackpackCrudRelationshipGrid\Tests;

use GertMark\BackpackCrudRelationshipGrid\BackpackCrudRelationshipGridServiceProvider;

class TestCase extends \Orchestra\Testbench\TestCase
{
  public function setUp(): void
  {
    parent::setUp();
    // additional setup
  }

  protected function getPackageProviders($app)
  {
    return [
      BackpackCrudRelationshipGridServiceProvider::class,
    ];
  }

  protected function getEnvironmentSetUp($app)
  {
    // perform environment setup
  }
}
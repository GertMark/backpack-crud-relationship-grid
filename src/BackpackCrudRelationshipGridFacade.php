<?php

namespace GertMark\BackpackCrudRelationshipGrid;

use Illuminate\Support\Facades\Facade;

/**
 * @see \GertMark\BackpackCrudRelationshipGrid\Skeleton\SkeletonClass
 */
class BackpackCrudRelationshipGridFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'backpack_crud_relationship_grid';
    }
}

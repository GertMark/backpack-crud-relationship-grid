<?php

namespace GertMark\BackpackCrudRelationshipGrid\Http\Controllers\Operations;


trait GridOperation
{

	public function handleRelationModelController($controllerName,$namespace){
        $controller = $namespace.'\\'.$controllerName;
        $this->$controllerName = new $controller();
        if($this->crud->getOperation()=='list'){
            return;
        }
        if(!method_exists($controller, 'setupGrid')){
            return;
        }
        if(!method_exists($controller, 'setupListOperationGrid')){
            return;
        }
        $this->$controllerName->setupGrid();
        $this->$controllerName->setupListOperationGrid();
    }

    public function setupGrid()
    {
        app()->singleton('crud', function ($app) {
            return new \Backpack\CRUD\app\Library\CrudPanel\CrudPanel($app);
        });
        $this->crud = app('crud');
        $this->crud->setModel($this->model);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/'.$this->entity);
        $this->crud->setEntityNameStrings($this->entityName, $this->entityNames);
    }

    public function setupListOperationGrid(){
        $this->setupListOperation();
        $this->crud->addButton('line', 'update', 'view', 'crud::buttons.update', 'end');
    }

    public function getCrud(){
        return $this->crud;
    }

    public static function getFieldsArrayForGridTab($ModelCrudController,$crudPanel,$tabName,$entity,$include_main_form_fields=[],$active = true)
    {
        return [
            [
                'name'              => $entity,
                'label'             => 'Titel',
                'type'              => 'relationship',
                'relation_type'     => 'hasMany',
                'presentation'      => ['presentation'=>'grid',
                                        'controller' => $ModelCrudController,
                                        'crudPanel' => $crudPanel,
                                        'include_main_form_fields' => $include_main_form_fields,
                                        ],
                'entity'            => $entity,
                'tab'               => $tabName,
                'attribute'         => "text",
                'inline_create' => [ // specify the entity in singular
                    'entity' => $entity, // the entity in singular
                    // OPTIONALS
                    'force_select' => true, // should the inline-created entry be immediately selected?
                    'modal_class' => 'modal-dialog modal-xl', // use modal-sm, modal-lg to change width
                    'modal_route' => route($entity.'-inline-create'), // InlineCreate::getInlineCreateModal()
                    'create_route' =>  route($entity.'-inline-create-save'), // InlineCreate::storeInlineCreate()
                    'include_main_form_fields' => $include_main_form_fields, // pass certain fields from the main form to the modal
                    'controller' => $ModelCrudController,
                    'crudPanel' => $crudPanel,
                    'active' => $active
                ],

                //'data_source' => url("manager/fetch/questionsSelectList"), // url to controller search function (with /{id} should return model)
                'wrapperAttributes' => [
                    'class' => 'form-group col-md-12',
                ],
            ]
        ];
    }

}
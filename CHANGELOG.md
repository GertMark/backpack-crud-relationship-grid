# Changelog

All notable changes to `backpack_crud_relationship_grid` will be documented in this file

## 1.0.0 - 201X-XX-XX

- initial release

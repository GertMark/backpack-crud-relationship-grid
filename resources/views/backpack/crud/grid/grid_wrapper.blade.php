<!--  relationship  -->

@php

    //in case entity is superNews we want the url friendly super-news
    $entityWithoutAttribute = $crud->getOnlyRelationEntity($field);
    $routeEntity = Str::kebab($entityWithoutAttribute);
    $connected_entity = new $field['model'];
    $connected_entity_key_name = $connected_entity->getKeyName();

    // make sure the $field['value'] takes the proper value
    // and format it to JSON, so that select2 can parse it
    $current_value = old(square_brackets_to_dots($field['name'])) ?? old($field['name']) ?? $field['value'] ?? $field['default'] ?? '';
    if ($current_value != false) {
        switch (gettype($current_value)) {
            case 'array':
                $current_value = $connected_entity
                                    ->whereIn($connected_entity_key_name, $current_value)
                                    ->get()
                                    ->pluck($field['attribute'], $connected_entity_key_name)
                                    ->toArray();
                break;
            case 'object':
            if (is_subclass_of(get_class($current_value), 'Illuminate\Database\Eloquent\Model') ) {
                    $current_value = [$current_value->{$connected_entity_key_name} => $current_value->{$field['attribute']}];
                }else{
                    if(! $current_value->isEmpty())  {
                    $current_value = $current_value
                                    ->pluck($field['attribute'], $connected_entity_key_name)
                                    ->toArray();
                    }
                }
            break;
            default:
                $current_value = $connected_entity
                                ->where($connected_entity_key_name, $current_value)
                                ->get()
                                ->pluck($field['attribute'], $connected_entity_key_name)
                                ->toArray();

                break;
        }
    }

    $field['value'] = json_encode($current_value);


    $field['data_source'] = $field['data_source'] ?? url($crud->route.'/fetch/'.$routeEntity);
    $field['include_all_form_fields'] = $field['include_all_form_fields'] ?? true;
    $field['attributes'] = $field['attributes'] ?? [];
    $field['attributes']['class'] = $field['attributes']['class'] ?? $default_class ?? 'form-control';
    $field['inline_create']['main_form_fields_data'] = '';




//$activeInlineCreate = !empty($field['inline_create']) ? true : false;
    $activeInlineCreate = empty($field['inline_create']) ? false : $field['inline_create']['active'];

 

if($activeInlineCreate) {


    //we check if this field is not beeing requested in some InlineCreate operation.
    //this variable is setup by InlineCreate modal when loading the fields.
    if(!isset($inlineCreate)) {
        //by default, when creating an entity we want it to be selected/added to selection.
        $field['inline_create']['force_select'] = $field['inline_create']['force_select'] ?? true;

        $field['inline_create']['modal_class'] = $field['inline_create']['modal_class'] ?? 'modal-dialog';

        //if user don't specify a different entity in inline_create we assume it's the same from $field['entity'] kebabed
        $field['inline_create']['entity'] = $field['inline_create']['entity'] ?? $routeEntity;

        //route to create a new entity
        $field['inline_create']['create_route'] = route($field['inline_create']['entity']."-inline-create-save");

        //route to modal
        $field['inline_create']['modal_route'] = route($field['inline_create']['entity']."-inline-create");

        $field['inline_create']['include_main_form_fields'] = $field['inline_create']['include_main_form_fields'] ?? [];

        $field['inline_create']['main_form_fields_data'] = '';

        if(count($field['inline_create']['include_main_form_fields']) > 0){
            $valueArray = [];
            foreach ($field['inline_create']['include_main_form_fields'] as $key => $name) {
                $valueArray[$name] = $crud->entry[$name];
            }
            $field['inline_create']['main_form_fields_data'] = json_encode($valueArray);
        }
        $valuesArray = json_decode($field['value']);
    }
}


@endphp

@include('crud::fields.inc.wrapper_start')

        <label>{!! $field['label'] !!}</label>
        @include('crud::fields.inc.translatable_icon')

        @if($activeInlineCreate)
            @include('crud::fields.relationship.inline_create_button', ['field' => $field])
        @endif


    @include('crud::grid.grid',[ 'crud' => $crud,
                                'controller' => $field['presentation']['controller'],
                                'crudPanel' => $field['presentation']['crudPanel'],
                                'title' => $crud->getTitle()])



  <!-- DATA TABLES -->
  <link rel="stylesheet" type="text/css" href="{{ asset('packages/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('packages/datatables.net-fixedheader-bs4/css/fixedHeader.bootstrap4.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('packages/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}">

  <link rel="stylesheet" href="{{ asset('packages/backpack/crud/css/crud.css') }}">
  <link rel="stylesheet" href="{{ asset('packages/backpack/crud/css/form.css') }}">
  <link rel="stylesheet" href="{{ asset('packages/backpack/crud/css/list.css') }}">

  <!-- CRUD LIST CONTENT - crud_list_styles stack -->
  @stack('crud_list_styles')

  @include(backpack_view('inc.scripts'))

  @include('crud::inc.grid_datatables_logic')
  <script src="{{ asset('packages/backpack/crud/js/crud.js') }}"></script>
  <script src="{{ asset('packages/backpack/crud/js/form.js') }}"></script>
  <script src="{{ asset('packages/backpack/crud/js/list.js') }}"></script>

  <!-- CRUD LIST CONTENT - crud_list_scripts stack -->
  @stack('crud_list_scripts')

  @include('crud::fields.inc.wrapper_end')

        @if ($crud->fieldTypeNotLoaded($field))
        @php
            $crud->markFieldTypeAsLoaded($field);
        @endphp

        {{-- FIELD CSS - will be loaded in the after_styles section --}}


        {{-- FIELD JS - will be loaded in the after_scripts section --}}
        @push('crud_fields_scripts')

           
            <script>

document.styleSheets[0].addRule('.select2-selection__clear::after','content:  "{{ trans('backpack::crud.clear') }}";');




//this setup the "+Add" button in page with corresponding click handler.
//when clicked, fetches the html for the modal to show

function setupInlineCreateButtonsGrid(obj) {
    var $fieldEntity = obj.data_field_related_name;
    var $inlineCreateButtonElement = $('.inline-create-button');
    var $inlineModalRoute = obj.data_inline_modal_route;
    var $inlineModalClass = obj.data_inline_modal_class;
    var $parentLoadedFields = obj.data_parent_loaded_fields;
    var $MainFormFieldsData = JSON.parse(obj.data_main_form_fields_data);
    $inlineCreateButtonElement.on('click', function () {

        //we change button state so users know something is happening.
        var loadingText = '<span class="la la-spinner la-spin" style="font-size:18px;"></span>';
        if ($inlineCreateButtonElement.html() !== loadingText) {
            $inlineCreateButtonElement.data('original-text', $inlineCreateButtonElement.html());
            $inlineCreateButtonElement.html(loadingText);


        }
        var datatemp = {
            'entity': $fieldEntity,
            'modal_class' : $inlineModalClass,
            'parent_loaded_fields' : $parentLoadedFields,
        }
        var data = Object.assign(datatemp, $MainFormFieldsData);
            
        $.ajax({
            url: $inlineModalRoute,
            data: data,
            type: 'POST',
            success: function (result) {
                $('body').append(result);
                triggerModal(obj);

            },
            error: function (result) {
                // Show an alert with the result
                swal({
                    title: "error",
                    text: "error",
                    icon: "error",
                    timer: 4000,
                    buttons: false,
                });
            }
        });

    });

}

// when an entity is created we query the ajax endpoint to check if the created option is returned.
function ajaxSearch(element, created) {
    var $relatedAttribute = element.attr('data-field-attribute');
    var $relatedKeyName = element.attr('data-connected-entity-key-name');
    var $searchString = created[$relatedAttribute];
    var $appLang = element.attr('data-app-current-lang');

    //we run the promise with ajax call to search endpoint to check if we got the created entity back
    //in case we do, we add it to the selected options.
    performAjaxSearch(element, $searchString).then(result => {
        var inCreated = $.map(result.data, function (item) {
            var $itemText = processItemText(item, $relatedAttribute, $appLang);
            var $createdText = processItemText(created, $relatedAttribute, $appLang);
            if($itemText == $createdText) {
                    return {
                        text: $itemText,
                        id: item[$relatedKeyName]
                    }
                }
        });

        if(inCreated.length) {
            selectOption(element, created);
        }
    });
}

//this is the function called when button to add is pressed,
//it triggers the modal on page and initialize the fields

function triggerModal(obj) {
    var $fieldName = obj.data_field_related_name;
    var $modal = $('#inline-create-dialog');
    var $modalSaveButton = $modal.find('#saveButton');
    var $modalCancelButton = $modal.find('#cancelButton');
    var $form = $(document.getElementById($fieldName+"-inline-create-form"));
    var $inlineCreateRoute = obj.data_inline_create_route;
    var $ajax = obj.data_field_ajax == 'true' ? true : false;
    var $force_select = (obj.data_force_select == 'true') ? true : false;


    $modal.modal();

    initializeFieldsWithJavascript($form);

    $modalCancelButton.on('click', function () {
        $($modal).modal('hide');
    });

    //when you hit save on modal save button.
    $modalSaveButton.on('click', function () {

        $form = document.getElementById($fieldName+"-inline-create-form");

        //this is needed otherwise fields like ckeditor don't post their value.
        $($form).trigger('form-pre-serialize');

        var $formData = new FormData($form);

        //we change button state so users know something is happening.
        //we also disable it to prevent double form submition
        var loadingText = '<i class="la la-spinner la-spin"></i> saving...';
        if ($modalSaveButton.html() !== loadingText) {
            $modalSaveButton.data('original-text', $(this).html());
            $modalSaveButton.html(loadingText);
            $modalSaveButton.prop('disabled', true);
        }


        $.ajax({
            url: $inlineCreateRoute,
            data: $formData,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (result) {

                $createdEntity = result.data;
                crud.table.ajax.reload();

                // if(!$force_select) {
                //     //if developer did not force the created entity to be selected we first try to
                //     //check if created is still available upon model re-search.
                //     ajaxSearch(element, result.data);

                // }else{
                //     selectOption(element, result.data);
                // }

                $modal.modal('hide');



                new Noty({
                    type: "info",
                    text: '{{ trans('backpack::crud.related_entry_created_success') }}',
                }).show();
            },
            error: function (result) {

                var $errors = result.responseJSON.errors;

                let message = '';
                for (var i in $errors) {
                    message += $errors[i] + ' \n';
                }

                new Noty({
                    type: "error",
                    text: '<strong>{{ trans('backpack::crud.related_entry_created_error') }}</strong><br> '+message,
                }).show();

                //revert save button back to normal
                $modalSaveButton.prop('disabled', false);
                $modalSaveButton.html($modalSaveButton.data('original-text'));
            }
        });
    });

    $modal.on('hidden.bs.modal', function (e) {
        $modal.remove();

        //when modal is closed (canceled or success submited) we revert the "+ Add" loading state back to normal.
        var $inlineCreateButtonElement = $('.inline-create-button');
        $inlineCreateButtonElement.html($inlineCreateButtonElement.data('original-text'));
    });


    $modal.on('shown.bs.modal', function (e) {
        $modal.on('keyup',  function (e) {
        if($modal.is(':visible')) {
            var key = e.which;
                if (key == 13) { //This is an ENTER
                e.preventDefault();
                $modalSaveButton.click();
            }
        }
        return false;
    });
    });
}





function bpFieldInitFetchOrCreateElement(element) {
    var form = element.closest('form');
    var $inlineField = element.attr('data-is-inline');
    var $ajax = element.attr('data-field-ajax') == 'true' ? true : false;
    var $placeholder = element.attr('data-placeholder');
    var $minimumInputLength = element.attr('data-minimum-input-length');
    var $dataSource = element.attr('data-data-source');
    var $method = element.attr('data-method');
    var $fieldAttribute = element.attr('data-field-attribute');
    var $connectedEntityKeyName = element.attr('data-connected-entity-key-name');
    var $includeAllFormFields = element.attr('data-include-all-form-fields')=='false' ? false : true;
    var $dependencies = JSON.parse(element.attr('data-dependencies'));
    var $modelKey = element.attr('data-model-local-key');
    var $allows_null = (element.attr('data-allows-null') == 'true') ? true : false;
    var $appLang = element.attr('data-app-current-lang');
    var $selectedOptions = JSON.parse(element.attr('data-selected-options') ?? null);
    var $multiple = element.prop('multiple');

    var FetchOrCreateAjaxFetchSelectedEntry = function (element) {
            return new Promise(function (resolve, reject) {
                $.ajax({
                    url: $dataSource,
                    data: {
                        'keys': $selectedOptions
                    },
                    type: $method,
                    success: function (result) {

                        resolve(result);
                    },
                    error: function (result) {
                        reject(result);
                    }
                });
            });
        };

       if($allows_null && !$multiple) {
        $(element).append('<option value="">'+$placeholder+'</option>');
       }

        if (typeof $selectedOptions !== typeof undefined &&
            $selectedOptions !== false &&
                $selectedOptions != '' &&
                $selectedOptions != null &&
                $selectedOptions != [])
        {
            var optionsForSelect = [];

            FetchOrCreateAjaxFetchSelectedEntry(element).then(result => {
                result.forEach(function(item) {
                $itemText = processItemText(item, $fieldAttribute, $appLang);
                $itemValue = item[$connectedEntityKeyName];
                //add current key to be selected later.
                optionsForSelect.push($itemValue);

                //create the option in the select
                $(element).append('<option value="'+$itemValue+'">'+$itemText+'</option>');
                    });

                    // set the option keys as selected.
                    $(element).val(optionsForSelect);
                    $(element).trigger('change');
                });
            }

            var $item = false;

            var $value = JSON.parse(element.attr('data-current-value'))

            

            if(Object.keys($value).length > 0) {
                $item = true;
            }
            var selectedOptions = [];
            var $currentValue = $item ? $value : '';

            //we reselect the previously selected options if any.
            for (const [key, value] of Object.entries($currentValue)) {
                selectedOptions.push(key);
                var $option = new Option(value, key);
                $(element).append($option);
            }

            $(element).val(selectedOptions);

            //null is not allowed we fetch some default entry
            if(!$allows_null && !$item && $selectedOptions == null) {
                fetchDefaultEntry(element).then(result => {
                    $(element).append('<option value="'+result[$modelKey]+'">'+result[$fieldAttribute]+'</option>');
                    $(element).val(result[$modelKey]);
                    $(element).trigger('change');
                });
            }



        //Checks if field is not beeing inserted in one inline create modal and setup buttons
        if($inlineField == "false") {
            setupInlineCreateButtons(element);
        }

            if (!element.hasClass("select2-hidden-accessible")) {


                    element.select2({
                    theme: "bootstrap",
                    placeholder: $placeholder,
                    minimumInputLength: $minimumInputLength,
                    allowClear: $allows_null,
                    ajax: {
                    url: $dataSource,
                    type: $method,
                    dataType: 'json',
                    quietMillis: 500,
                    data: function (params) {
                    if ($includeAllFormFields) {
                    return {
                        q: params.term, // search term
                        page: params.page, // pagination
                        form: form.serializeArray() // all other form inputs
                    };
                } else {
                    return {
                        q: params.term, // search term
                        page: params.page, // pagination
                    };
                }
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                //if we have data.data here it means we returned a paginated instance from controller.
                //otherwise we returned one or more entries unpaginated.
                if(data.data) {
                var result = {
                    results: $.map(data.data, function (item) {
                        var $itemText = processItemText(item, $fieldAttribute, $appLang);

                        return {
                            text: $itemText,
                            id: item[$connectedEntityKeyName]
                        }
                    }),
                    pagination: {
                            more: data.current_page < data.last_page
                    }
                };
                }else {
                    var result = {
                        results: $.map(data, function (item) {
                            var $itemText = processItemText(item, $fieldAttribute, $appLang);

                            return {
                                text: $itemText,
                                id: item[$connectedEntityKeyName]
                            }
                        }),
                        pagination: {
                            more: false,
                        }
                    }
                }

                return result;
            },
            cache: true
        },
                });
            }

        for (var i=0; i < $dependencies.length; i++) {
        $dependency = $dependencies[i];
        $('input[name='+$dependency+'], select[name='+$dependency+'], checkbox[name='+$dependency+'], radio[name='+$dependency+'], textarea[name='+$dependency+']').change(function () {
            element.val(null).trigger("change");
        });
    }

        }
if (typeof processItemText !== 'function') {
    function processItemText(item, $fieldAttribute, $appLang) {
        if(typeof item[$fieldAttribute] === 'object' && item[$fieldAttribute] !== null)  {
                if(item[$fieldAttribute][$appLang] != 'undefined') {
                    return item[$fieldAttribute][$appLang];
                }else{
                    return item[$fieldAttribute][0];
                }
            }else{
                return item[$fieldAttribute];
            }
}
}

$(document).ready(function(){
    var dataObj = {
                    "name":"{{ $field['name'].($field['multiple']?'[]':'') }}",
                    "data_original_name":"{{ $field['name'] }}",
                    "style":"width: 100%",
                    "data_force_select":"{{ var_export($field['inline_create']['force_select']) }}",
                    "data_init_function":"bpFieldInitFetchOrCreateElement",
                    "data_is_inline":"{{ $inlineCreate ?? 'false' }}",
                    "data_allows_null":"{{var_export($field['allows_null'])}}",
                    "data_dependencies":'{!! isset($field['dependencies'])?json_encode(Arr::wrap($field['dependencies'])): json_encode([]) !!}',
                    "data_model_local_key":"{{$crud->model->getKeyName()}}",
                    "data_placeholder":"{{ $field['placeholder'] }}",
                    "data_data_source":"{{ $field['data_source'] }}",
                    "data_method":"{{ $field['method'] ?? 'POST' }}",
                    "data_minimum_input_length":"{{ $field['minimum_input_length'] }}",
                    "data_field_attribute":"{{ $field['attribute'] }}",
                    "data_connected_entity_key_name":"{{ $connected_entity_key_name }}",
                    "data_include_all_form_fields":"{{ var_export($field['include_all_form_fields']) }}",
                    "data_current_value":"{{ $field['value'] }}",
                    "data_field_ajax":"{{var_export($field['ajax'])}}",
                    "data_inline_modal_class":"{{ $field['inline_create']['modal_class'] }}",
                    "data_app_current_lang":"{{ app()->getLocale() }}",
                    "data_main_form_fields_data":'{!! $field['inline_create']['main_form_fields_data'] !!}',
                    "data_inline_create_route":"{{$field['inline_create']['create_route'] ?? false}}",
                    "data_inline_modal_route":"{{$field['inline_create']['modal_route'] ?? false}}",
                    "data_field_related_name":"{{$field['inline_create']['entity']}}",
                    "data_inline_create_button":"{{ $field['inline_create']['entity'] }}-inline-create-{{$field['name']}}",
                    "data_inline_allow_create":"{{var_export($activeInlineCreate)}}",
                    "data_parent_loaded_fields":'{!! json_encode(array_unique(array_column($crud->fields(),'type'))) !!}',
                    
                    @foreach ($field['attributes'] as $attribute => $value)
                        @if (is_string($attribute))
                        "{{ $attribute }}":"{{ $value }}",
                        @endif
                    @endforeach

            }
    setupInlineCreateButtonsGrid(dataObj);
})
            </script>
        @endpush

    @endif


    {{-- End of Extra CSS and JS --}}
    {{-- ########################################## --}}


<?php

namespace GertMark\BackpackCrudRelationshipGrid;

use Illuminate\Support\ServiceProvider;
use GertMark\BackpackCrudRelationshipGrid\Console\InstallBackpackCrudRelationshipGridPackage;
use GertMark\BackpackCrudRelationshipGrid\Console\InitRelationGridCommand;

class BackpackCrudRelationshipGridServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        /*
         * Optional methods to load your package assets
         */
        // $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'backpack_crud_relationship_grid');
        // $this->loadViewsFrom(__DIR__.'/../resources/views', 'backpack_crud_relationship_grid');
        // $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        // $this->loadRoutesFrom(__DIR__.'/routes.php');

        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../config/config.php' => config_path('backpack_crud_relationship_grid.php'),
            ], 'config');

            // Publishing the views.
            /*$this->publishes([
                __DIR__.'/../resources/views' => resource_path('views/vendor/backpack_crud_relationship_grid'),
            ], 'views');*/
            $this->publishes([
                __DIR__.'/../resources/views/backpack/crud' => resource_path('views/vendor/backpack/crud'),
            ], 'backpack_grid_views');

            // Publishing assets.
            /*$this->publishes([
                __DIR__.'/../resources/assets' => public_path('vendor/backpack_crud_relationship_grid'),
            ], 'assets');*/

            // Publishing the translation files.
            /*$this->publishes([
                __DIR__.'/../resources/lang' => resource_path('lang/vendor/backpack_crud_relationship_grid'),
            ], 'lang');*/

            // Registering package commands.
            $this->commands([
                InstallBackpackCrudRelationshipGridPackage::class,
                InitRelationGridCommand::class
            ]);

        }
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        // Automatically apply the package configuration
        $this->mergeConfigFrom(__DIR__.'/../config/config.php', 'backpack_crud_relationship_grid');

        // Register the main class to use with the facade
        $this->app->singleton('backpack_crud_relationship_grid', function () {
            return new BackpackCrudRelationshipGrid;
        });

    }
}
